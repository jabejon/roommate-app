// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCYvwVjeq4yjv34QnflU21NRJJqnXfoftE",
    authDomain: "roommateapp-ec87d.firebaseapp.com",
    projectId: "roommateapp-ec87d",
    storageBucket: "roommateapp-ec87d.appspot.com",
    messagingSenderId: "814741610002",
    appId: "1:814741610002:web:3bc85f08274a3c6ed902cb",
    measurementId: "G-W4LXS5RBHZ"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
