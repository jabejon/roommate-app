import { Component } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  currentUser: any;

  constructor(
    private auth: AuthService,
    private navCtrl: NavController,
    private router: Router
  ) {}

  ngOnInit() {
    this.currentUser = this.auth.currentUser;
    this.hasMissingInfoUser(this.currentUser);
  }

  logout() {
    this.auth.logout();
		this.router.navigateByUrl('login', { replaceUrl: true })
  }

  navigateToUserInformation() {
    this.navCtrl.navigateForward('user-information');
  }

  hasMissingInfoUser(user: any) {
		if (!user.displayName)
      this.navigateToUserInformation();
	}

}
