import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { FirebaseStorageService } from 'src/app/services/firebase-storage.service';

@Component({
  selector: 'app-find-roommate',
  templateUrl: './find-roommate.component.html',
  styleUrls: ['./find-roommate.component.scss'],
})
export class FindRoommateComponent  implements OnInit {
  form!: FormGroup;
  availableRooms: any[] = [];

  constructor(
    private storageService: FirebaseStorageService,
    private navCtrl: NavController,
  ) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.form = new FormGroup({
      minPrice: new FormControl(0),
      maxPrice: new FormControl(null),
      location: new FormControl(null),
      minAge: new FormControl(null),
      maxAge: new FormControl(null),
      enterDate: new FormControl(null),
      gender: new FormControl(null),
      allowPets: new FormControl(false)
    })
  }

  async applyFilters() {
    const availableRooms = await this.storageService.getRoomsByFilters(this.form.getRawValue());
    if (availableRooms && availableRooms.length > 0)
      this.availableRooms = availableRooms;
  }

  selectRoom(room: any) {
    if (room.hasTest)
      this.navCtrl.navigateForward('room-test-page', {queryParams: {userId: room.userId, test: room.test}});
    else
      this.navCtrl.navigateForward('room-details', {queryParams: room});
  }

}
