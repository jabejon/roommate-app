import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { FirebaseStorageService } from 'src/app/services/firebase-storage.service';

@Component({
  selector: 'app-add-room',
  templateUrl: './add-room.component.html',
  styleUrls: ['./add-room.component.scss'],
})
export class AddRoomComponent  implements OnInit {
  form!: FormGroup;
  
  constructor(
    private storageService: FirebaseStorageService,
    private authService: AuthService,
  ) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.form = new FormGroup({
      question1: new FormControl({question: '', answer1: '', answer2: '', answer3: '', correctAnswer: null}),
      question2: new FormControl({question: '', answer1: '', answer2: '', answer3: '', correctAnswer: null}),
      question3: new FormControl({question: '', answer1: '', answer2: '', answer3: '', correctAnswer: null}),
      question4: new FormControl({question: '', answer1: '', answer2: '', answer3: '', correctAnswer: null}),
      question5: new FormControl({question: '', answer1: '', answer2: '', answer3: '', correctAnswer: null}),
      question6: new FormControl({question: '', answer1: '', answer2: '', answer3: '', correctAnswer: null}),
      question7: new FormControl({question: '', answer1: '', answer2: '', answer3: '', correctAnswer: null}),
      question8: new FormControl({question: '', answer1: '', answer2: '', answer3: '', correctAnswer: null}),
      question9: new FormControl({question: '', answer1: '', answer2: '', answer3: '', correctAnswer: null}),
      question10: new FormControl({question: '', answer1: '', answer2: '', answer3: '', correctAnswer: null}),
    })
  }

  uploadTest() {
    this.storageService.uploadTest(this.form.getRawValue(), this.authService.userId!);
  }

}
