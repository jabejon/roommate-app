import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
import { CatastroService } from 'src/app/services/catastro.service';
import { FirebaseStorageService } from 'src/app/services/firebase-storage.service';
// import { Onfido, OnfidoConfig } from '@ionic-native/onfido/ngx';
// var exec = require('cordova/exec');

var PLUGIN_NAME = 'Onfido';

// var onFido = {
//     init: function (cb, options) {
//         exec(cb, null, PLUGIN_NAME, 'init', [options]);
//     }
// };

// module.exports = onFido;

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss'],
})
export class UserProfileComponent  implements OnInit {
  home: any;
  private readonly SDK_TOKEN = 'YOUR_SDK_TOKEN';
  private readonly APPLICANT_ID = 'YOUR_APPLICANT_ID';

  constructor(
    private navCtrl: NavController,
    private storageService: FirebaseStorageService,
    private auth: AuthService,
    private catastroService: CatastroService,
    // private onfido: Onfido
    ) { }

  ngOnInit() {
    this.getUserProfile();
    this.initHomeValues();
  }

  initHomeValues() {
    this.home = {
      province: 'Madrid',
      city: 'Madrid',
      streetType: 'cl',
      streetName: 'Gaztambide',
      streetNumber: '6',
      number: '01',
      door: 'B'
    }
  }

  // startVerification() {
  //   const config: OnfidoConfig = {
  //     token: this.SDK_TOKEN,
  //     applicantId: this.APPLICANT_ID,
  //     flowSteps: ['document', 'face'],
  //     onComplete: (data: any) => {
  //       console.log('Verification completed:', data);
  //     },
  //     onError: (error: any) => {
  //       console.error('Error during verification:', error);
  //     }
  //   };
  //   this.onfido.init(config);
  // }



  async getUserProfile() {
    // const user = await this.storageService.getUserProfile(this.auth.userId!);
    // console.log(user);    
  }

  async navigateToUserInformation() {
    this.navCtrl.navigateForward('user-information');
    // let response = await this.catastroService.getCatastro(this.home);
    // console.log(response);
  }

}
