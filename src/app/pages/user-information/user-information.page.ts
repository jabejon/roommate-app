import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FirebaseStorageService } from '../../services/firebase-storage.service';
import { AuthService } from '../../services/auth.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-user-information',
  templateUrl: './user-information.page.html',
  styleUrls: ['./user-information.page.scss']
})
export class UserInformationPage implements OnInit {
  form!: FormGroup;
  studies = [
    { name: 'Estudio', value: 1 },
    { name: 'Trabajo', value: 2 },
    { name: 'Ambos', value: 3 }
  ];

  genderOptions = [
    { name: 'Mujer', value: 1 },
    { name: 'Hombre', value: 2 },
    { name: 'No binaro', value: 3 }
  ];

  title: string = 'Información personal';
  compatibilityPercentage: number = 7;

  constructor(
    private storageService: FirebaseStorageService,
    private authService: AuthService,
    private navCtrl: NavController
  ) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.form = new FormGroup({
      name: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      birthDate: new FormControl(null, Validators.required),
      telNumber: new FormControl(null, Validators.required),
      gender: new FormControl(null, Validators.required),
      study: new FormControl(null, Validators.required)
    })
  }

  async saveUserInfo() {
    const userInfo = this.form.getRawValue();
    try {
      await this.authService.updateUser(userInfo);
      await this.storageService.updateUserInfo(userInfo, this.authService.userId!);
      this.navCtrl.pop();
    } catch (error) {
      console.log(error);
    }
  }

}
