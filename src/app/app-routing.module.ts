import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { redirectUnauthorizedTo, redirectLoggedInTo, canActivate } from '@angular/fire/auth-guard';
import { TabsComponent } from './components/tabs/tabs.component';
import { UserProfileComponent } from './pages/user-information/components/user-profile/user-profile.component';
import { AddRoomComponent } from './pages/add-room/add-room.component';
import { FindRoommateComponent } from './pages/find-roommate/find-roommate.component';

const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['']);
const redirectLoggedInToHome = () => redirectLoggedInTo(['tabs']);

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule),
    ...canActivate(redirectLoggedInToHome)
  },
  {
    path: 'tabs',
    component: TabsComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'home',
      },
      {
        path: 'home',
        loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule),
        ...canActivate(redirectUnauthorizedToLogin)
      },
      {
        path: 'user-profile',
        component: UserProfileComponent
      },
      {
        path: 'add-room',
        component: AddRoomComponent
      },
      {
        path: 'find-roommate',
        component: FindRoommateComponent
      },
    ]
  },
  {
    path: 'user-information',
    loadChildren: () => import('./pages/user-information/user-information.module').then( m => m.UserInformationPageModule)
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full'
  },
  {
    path: 'test',
    loadChildren: () => import('./pages/test/test.module').then( m => m.TestPageModule)
  },
  {
    path: 'test-result',
    loadChildren: () => import('./pages/test-result/test-result.module').then( m => m.TestResultPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
