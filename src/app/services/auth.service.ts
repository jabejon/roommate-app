import { Injectable } from '@angular/core';
import { Auth, signInWithEmailAndPassword,	createUserWithEmailAndPassword,	signOut, updateProfile, updatePhoneNumber} from '@angular/fire/auth';
import { FirebaseStorageService } from './firebase-storage.service';
// import { Storage } from '@ionic/storage-angular';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

constructor(
	private auth: Auth,
	private firestorageService: FirebaseStorageService
) {}

	get userId() {
		return this.auth.currentUser?.uid;
	}

	get currentUser() {
		return this.auth.currentUser;
	}

	async register({ email, password }: { email: string, password: string}) {
		try {
			const user = await createUserWithEmailAndPassword(this.auth, email, password);
			await this.firestorageService.addUser(user.user.email!, user.user.uid);
			return user;
		} catch (e) {
			return null;
		}
	}

	async login({ email, password }: { email: string, password: string}) {
		try {
			const user = await signInWithEmailAndPassword(this.auth, email, password);
			this.getTokenId();
			return user;
		} catch (e) {
			return null;
		}
	}

	logout() {
		return signOut(this.auth);
	}

	async updateUser(userInfo: any) {
		try {
			const user = await updateProfile(this.auth.currentUser!, { displayName: userInfo.name + ' ' + userInfo.lastName });
			return user;
		} catch (error) {
			return null;
		}
	}

	getTokenId() {
		this.auth.currentUser?.getIdTokenResult()
			.then(result => {
				console.log(result);
			})
			.catch(error => console.log(error));
	}
}
