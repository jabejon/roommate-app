import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CatastroService {

  constructor(
    private client: HttpClient
  ) { }

  getCatastro(homeData: any) {
    return this.client.get(
      'https://ovc.catastro.meh.es/OVCServWeb/OVCWcfCallejero/COVCCallejero.svc/json/Consulta_DNPLOC?Provincia='
      + homeData.province + '&Municipio=' + homeData.city + '&Sigla=' + homeData.streetType +
      '&Calle=' + homeData.streetName + '&Numero=' + homeData.streetNumber +'&Bloque=&Escalera=&Planta=' +
      homeData.number + '&Puerta=' + homeData.door
      ).toPromise();
  }

}
