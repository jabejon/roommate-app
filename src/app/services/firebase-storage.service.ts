import { Injectable } from '@angular/core';
import { Firestore, collection, collectionData, doc, docData, addDoc, deleteDoc, updateDoc, query, where, getDocs, getDoc, DocumentData, DocumentSnapshot } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class FirebaseStorageService {

  constructor(private firestore: Firestore) { }

  addUser(email: string, uid: string) {
    const usersRef = collection(this.firestore, 'users');
    return addDoc(usersRef, {email: email, id: uid});
  }

  async updateUserInfo(user: any, userId: string) {
    const querySnapshot = await this.getUserCollectionId(userId);
    querySnapshot.forEach((document) => {
      return updateDoc(doc(this.firestore, `users/${document.id}`), user);
    });
  }

  async getUserProfile(userId: string) {
    const querySnapshot = await this.getUserCollectionId(userId);
    querySnapshot.forEach((document) => {
      return getDoc(doc(this.firestore, 'users', document.id));
    });
  }

  async AddRoom(room: any, userId: string) {
    const roomsRef = collection(this.firestore, 'rooms');
    return addDoc(roomsRef, {room: room, id: userId});
  }

  async getUserCollectionId(userId: string) {
    const usersRef = collection(this.firestore, `users`);
    const q = query(usersRef, where("id", "==", userId));
    const querySnapshot = await getDocs(q);
    return querySnapshot;
  }

  async getRoomsByFilters(filters: any): Promise<any[]> {
    const roomsRef = collection(this.firestore, `rooms`);
    const q = query(roomsRef, where("minPrice", "==", filters.minPrice),
                              where("maxPrice", "==", filters.maxPrice),
                              where("location", "==", filters.location),
                              where("minAge", "==", filters.minAge),
                              where("maxAge", "==", filters.maxAge),
                              where("enterDate", "==", filters.enterDate),
                              where("gender", "==", filters.gender),
                              where("allowPets", "==", filters.allowPets));
    const querySnapshot = await getDocs(q);
    return querySnapshot.docs;
  }

  async getRoomTestEvaluation(userId: string, userTestAnswer: any) {
    const querySnapshot = await this.getOwnerTestId(userId);
    const ownerTestAnswers = this.getOwnerTestAnswer(querySnapshot);
    const correctAnswers = this.evaluateTest(ownerTestAnswers, userTestAnswer);
    return correctAnswers >= 7 ? true : false;
  }

  async getOwnerTestId(userId: string) {
    const testRef = collection(this.firestore, `test`);
    const q = query(testRef, where("id", "==", userId));
    const querySnapshot = await getDocs(q);
    return querySnapshot;
  }

  getOwnerTestAnswer(querySnapshot: any) {
    let ownerTestAnswers: Promise<DocumentSnapshot<DocumentData>>[] = [];
    querySnapshot.forEach((document: { id: string; }) => {
      ownerTestAnswers.push(getDoc(doc(this.firestore, 'test', document.id)));
    });
    return ownerTestAnswers;
  }

  evaluateTest(ownerTestAnswers: any[], userTestAnswer: any[]) {
    let correctAnswers: number = 0;
    userTestAnswer.forEach(answer => {
      let answerFound = ownerTestAnswers.find(ownerAnswer => ownerAnswer === answer);
      if (answerFound)
        correctAnswers++;
    });
    return correctAnswers;
  }

  getAllRooms() {
    const roomsRef = collection(this.firestore, 'rooms');
    return collectionData(roomsRef, { idField: 'id' });
  }  

  getAllUsers() {
    const usersRef = collection(this.firestore, 'users');
    return collectionData(usersRef, { idField: 'id' });
  }

  uploadTest(test:any, userId: string) {
    const testRef = collection(this.firestore, 'test');
    return addDoc(testRef, {test: test, id: userId});
  }

}
